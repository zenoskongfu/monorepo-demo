import { Button } from "components";
import "./App.css";

function App() {
	return (
		<>
			<Button.Search />
			<Button content='button from playground 1' />
			<Button content='button from playground 2' />
		</>
	);
}

export default App;
