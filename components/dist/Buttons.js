import React from 'react';
import Search from './Search.js';

var Button = function (props) {
  return /*#__PURE__*/React.createElement("button", null, props.content || "i am a button");
};
Button.Search = Search;

export { Button as default };
//# sourceMappingURL=Buttons.js.map
