function c(t, e, n, o, a) {
	if (
		!h.any(E, function (t) {
			return t(goldlog) === !1;
		})
	)
		return (
			"POST" !== o && "WS" !== o && "WS-ONLY" !== o && (o = "GET"),
			j.run(
				{ recordType: x.recordTypes.hjlj, method: o },
				{ logkey: t, gmkey: e, gokey: n },
				{ fn_after_record: I },
				function () {
					"function" == typeof a && a();
				}
			),
			!0
		);
}
