import React from "react";
import Search from "./Search";

const Button = (props: { content: string }) => {
	return <button>{props.content || "i am a button"}</button>;
};

Button.Search = Search;

export default Button;
