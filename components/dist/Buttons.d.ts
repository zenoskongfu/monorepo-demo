import React from "react";
declare const Button: {
    (props: {
        content: string;
    }): React.JSX.Element;
    Search: () => React.JSX.Element;
};
export default Button;
